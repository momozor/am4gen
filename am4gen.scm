(use-modules (json)
             (srfi srfi-1)
             (ice-9 format))

(define JSON-CONFIG-FILE-NAME "am4gen.json")
(define CONFIGURE-AC-FILE-NAME "configure.test")

(define (write-to-config data file)
  (call-with-output-file file
    (lambda (port)
      (display data port))))

(define (assemble-configure-files)
  (let* ((data
         (read-from-json-file
          JSON-CONFIG-FILE-NAME))
         
         (name (get-val data "name"))
         (version-major (get-val data "versionMajor"))
         (version-minor (get-val data "versionMinor"))
         (version-patch (get-val data "versionPatch"))
         (error-on-warning
          (get-val data "errorOnWarning"))
         (is-foreign
          (get-val data "isForeign"))
         (lang-standard
          (get-val data "langStandard"))
         (source-directory
          (get-val data "sourceDirectory"))
         
         (assemble-configure-ac
          (format #f
            "AC_INIT([~a], [~a.~a.~a], [])~%AM_INIT_AUTOMAKE([-Wall ~a ~a])~%~a~%AC_CONFIG_FILES(Makefile ~a/Makefile)~%"
            name version-major version-minor version-patch
            (if (string= error-on-warning "true")
                "-Werror"
                "")
            (if (string= is-foreign "true")
                "-foreign"
                "")
            (if (string= lang-standard "c99")
                "AC_PROG_CC_STDC"
                "")
            (if (string= source-directory "")
                "src"
                source-directory)))
         (assemble-root-makefile-am
          (format #f
                  "SUBDIRS = ~a~%"
                  source-directory)))
    
    (mkdir source-directory)
    (write-to-config assemble-configure-ac "configure.test")
    (write-to-config assemble-root-makefile-am "Makefile.test")))

(define (get-val object key)
  (cdr (assoc key object string-ci=?)))

(define (read-from-json-file file-name)
  (let ((f (open-input-file file-name)))
    (json->scm f)))
